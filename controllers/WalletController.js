import { createError } from "../error.js";
import * as strongSoap from "strong-soap";

export const recargar = async (req, res) => {
    var url = process.env.WSDL;
    var documento = await req.body.documento;
    var celular = await req.body.celular;
    var monto   = await req.body.monto;
    var soap = strongSoap.soap;

    var args = { "tns:request": {documento:documento, celular:celular, monto:monto}};
    
    var options = {};
    soap.createClient(url, options, function(err, client) {
      client.RecargarDineroService(args, function(err, result, envelope, soapHeader) {
        if (err)
        {
            console.log(err);
         } else
         {
             console.log(JSON.stringify(result));
             return res.status(200).json(result);
          }
      });
    });
}

export const consultar = async (req, res) => {
    var url = process.env.WSDL;
    var documento = await req.body.documento;
    var celular = await req.body.celular;
    var soap = strongSoap.soap;

    var args = { "tns:request": {documento:documento, celular:celular}};
    
    var options = {};
    await soap.createClient(url, options, function(err, client) {
       client.ConsultarSaldoService(args, function(err, result) {
        if (err)
        {
            console.log(err);
         } else
         {
             console.log(JSON.stringify(result));
             return res.status(200).json(result);
          }
      });
    });
    
}

