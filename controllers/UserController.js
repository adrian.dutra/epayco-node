import { createError } from "../error.js";
import * as strongSoap from "strong-soap";

export const create = async (req, res) => {
    var url = process.env.WSDL;
    var nombres = await req.body.nombres;
    var documento = await req.body.documento;
    var celular = await req.body.celular;
    var email   = await req.body.email;
    var soap = strongSoap.soap;

    var args = { "tns:request": {nombres :nombres, documento:documento, celular:celular, email:email}};
    
    var options = {};
    
    soap.createClient(url, options, function(err, client) {
        client.RegistrarUsuarioService(args, function(err, result, envelope, soapHeader) {
        if (err)
        {
            console.log(err);
         } else
         {
             console.log(JSON.stringify(result));
             return res.status(200).json(result);
          }
      });
    });
}


