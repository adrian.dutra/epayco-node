import express from "express";
import cors from "cors";
import { config } from "dotenv";
import userRoutes from "./routes/users.js";
import walletRoutes from "./routes/wallet.js";
import cookieParser from "cookie-parser";

const app = express();
config();
const port = process.env.PORT || 3000;


app.use(cookieParser());
app.use(express.json());
app.use(cors());

//app.use('/users', users);

app.use('/api/users', userRoutes);
app.use('/api/wallet', walletRoutes);

app.use((err, req, res, next) => {
  const status = err.status || 500;
  const message = err.message || "Something went wrong";
  return res.status(status).json ({
     success: false,
     status,
     message,
  })
});


app.listen(port, () => {
    console.log("Connect!")
});