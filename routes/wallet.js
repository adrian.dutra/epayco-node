import express from "express";
import { recargar, consultar } from "../controllers/WalletController.js";

const router = express.Router();

//Recargar Billetera
router.post('/recargar', recargar);

//Consultar Saldo
router.get('/consultar', consultar);


export default router;